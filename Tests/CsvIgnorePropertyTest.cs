﻿using CsvFileReader;
using CsvFileReader.Decorators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Tests.Model;

namespace Tests
{
    /// <summary>
    /// this class tests the ignore property decorator
    /// </summary>
    [TestClass()]
    public class CsvIgnorePropertyTest
    {
        [TestInitialize()]
        public void MyTestInitialize()
        {
            this.dataPath = "../../../Tests/TestData/";
        }
        string dataPath;

        /// <summary>
        /// test that the property is ignored. No way to just test the class
        /// </summary>
        [TestMethod()]
        public void IntegrationTest()
        {
            CsvFile target = new CsvFile(this.dataPath + "Comment.csv");
            List<ignoreModel> data = target.Reader.RetrieveData<ignoreModel>() as List<ignoreModel>;

            Assert.AreEqual("3202543554", data[0].Phone);
            Assert.AreEqual(null, data[0].OrgID);
        }
    }
}
