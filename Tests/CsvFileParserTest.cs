﻿using CsvFileReader.Models;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using CsvFileReaderTest.Model;

namespace CsvFileReaderTest
{
    /// <summary>
    ///This is a test class for CsvFileParserTest and is intended
    ///to contain all CsvFileParserTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CsvFileParserTest
    {
        /// <summary>
        ///A test for CsvFileParser ParseRow
        ///</summary>
        [TestMethod()]
        public void ParseRowEmptyFirstRowTest()
        {
            CsvFileParser target = new CsvFileParser(',');
            string row = ",\"tes,t\",more";
            List<string> rows = target.ParseRow(row);
            Assert.AreEqual(3, rows.Count);
            Assert.AreEqual(rows[0], "");
        }

        /// <summary>
        ///A test for CsvFileParser ParseRow with extra comma
        ///</summary>
        [TestMethod()]
        public void ParseRowEmptyLastRowTest()
        {
            CsvFileParser target = new CsvFileParser(',');
            string row = "\"tes,t\",more,";
            List<string> rows = target.ParseRow(row);
            Assert.AreEqual(3, rows.Count);
            Assert.AreEqual(rows[2], "");
        }
    }
}
