﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CsvFileReader.Decorators;

namespace CsvFileReaderTest.Model
{
    public class listGenericModel
    {
        [CsvListProperty(',')]
        public List<int> OrgID { get; set; }

        [CsvDate("MMddyyyy")]
        [CsvListProperty(',')]
        public List<DateTime> SID { get; set; }


        [CsvListProperty("SID", ',')]
        [CsvMapTo("SID")]
        public List<Comment> Obj { get; set; }
    }
}
