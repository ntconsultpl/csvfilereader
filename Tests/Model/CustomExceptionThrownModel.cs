﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CsvFileReader.Decorators;

namespace CsvFileReaderTest.Model
{
    public class CustomExceptionThrownModel
    {
        public int SID { get; set; }

        [CsvMapTo("OrgID")]
        public Comment CustomProp { get; set; }
    }
}

