﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CsvFileReader.Decorators;

namespace CsvFileReaderTest.Model
{
    public class CustomObject
    {
        public int OrgID { get; set; }

        [CsvListProperty("SID")]
        public Comment SID { get; set; }
    }
}
