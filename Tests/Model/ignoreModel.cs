﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CsvFileReader.Decorators;

namespace Tests.Model
{
    public class ignoreModel
    {
        [CsvIgnoreProperty]
        public String OrgID { get; set; }
        public String Phone { get; set; }
        public String AcctNum { get; set; }
    }
}
