﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CsvFileReader.Decorators;

namespace CsvFileReaderTest.Model
{
    public class AttributeTestModel
    {
        [CsvMapTo("OrgID")]
        public int propA { get; set; }
        [CsvMapTo("Phone")]
        public String propB { get; set; }
        [CsvMapTo("AcctNum")]
        public String propC { get; set; }

        [CsvMapTo("DisDate")]
        [CsvDate("MMddyyyy")]
        public DateTime D { get; set; }
    }
}
