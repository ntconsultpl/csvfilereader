﻿using System;
using System.Collections.Generic;

namespace CsvFileReader.Interfaces.Models
{
    /// <summary>
    /// this interface is for parsing files
    /// </summary>
    public interface IFileParse
    {
        /// <summary>
        /// parse row based on parser type
        /// </summary>
        /// <param name="str">is a<typeparamref name="String"/>. The String to parse</param>
        /// <returns></returns>
        List<String> ParseRow(String str);
    }
}
