﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsvFileReader
{
	public class MappingDefinition
	{
		public int Column { get; set; }
		public string PropertyName { get; set; }

		public MappingDefinition()
		{ }

		public MappingDefinition(int column, string propertyName)
		{
			this.Column = column;
			this.PropertyName = propertyName;
		}
	}
}
