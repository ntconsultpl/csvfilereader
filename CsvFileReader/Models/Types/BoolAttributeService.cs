﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using CsvFileReader.Decorators;
using CsvFileReader.Exceptions;
using CsvFileReader.Interfaces.Models;


namespace CsvFileReader.Models.Types
{
	/// <summary>
	/// Boolean type service resolves bool type values
	/// </summary>
	public class BoolAttributeService
	{
        #region Private Static Members
        private static BoolAttributeService instance;
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Singleton Instance
        /// </summary>
        /// <returns></returns>
        public static BoolAttributeService Retrieve()
        {
            return BoolAttributeService.instance;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Singleton Constructor
        /// </summary>
        static BoolAttributeService()
        {
			BoolAttributeService.instance = new BoolAttributeService();
        }
        /// <summary>
        /// Singleton class so a private constructor is needed
        /// </summary>
        private BoolAttributeService() { }
		#endregion

		#region Public Methods
		/// <summary>
		/// resolve bool types
		/// </summary>
		/// <param name="obj">This is the instantiated model that we are mapping properties to</param>
		/// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
		/// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
		public void ResolveBool(Object obj, PropertyInfo info, String contents)
        {
            if (String.IsNullOrWhiteSpace(contents))
            {
                //set default to false
                info.SetValue(obj, false, null);
            }
            else
            {
                info.SetValue(obj, bool.Parse(contents), null);
            }
        }
		/// <summary>
		/// resolve nullable bools
		/// </summary>
		/// <param name="obj">This is the instantiated model that we are mapping properties to</param>
		/// <param name="info">is a <typeparamref name="PropertyInfo"/>. The object info</param>
		/// <param name="contents">is a <typeparamref name="String"/>. The Value</param>
		public void ResolveNullableBool(Object obj, PropertyInfo info, string contents)
        {
            if (String.IsNullOrWhiteSpace(contents))
            {
                //set default to null
                info.SetValue(obj, null, null);
            }
            else
            {
                info.SetValue(obj, bool.Parse(contents), null);
            }
        }
        #endregion
    }
}
