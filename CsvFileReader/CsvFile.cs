﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CsvFileReader.Interfaces.Models;
using CsvFileReader.Models;

namespace CsvFileReader
{
    /// <summary>
    /// This is a container for using CsvReaders
    /// </summary>
    public class CsvFile
    {
        #region Public Properties
        public CsvReader Reader { get; private set; }
        public IFileParse Parser { get; private set; }
        public IDataAttribute Mapper { get; private set; }
		#endregion

		#region Constructor
		/// <summary>
		/// base constructor with a csv parser defined
		/// </summary>
		/// <param name="path">is a <typeparamref name="string"/> The path of the file</param>
		/// <param name="delimiter">is a <typeparamref name="char"/> The delimiter used in file. Default value is ','</param>
		public CsvFile(string path, char delimiter = ',')
        {
            this.Parser = new CsvFileParser(delimiter);
            this.Mapper = new DataTypeService();
            this.Reader = new CsvReader(path, this.Parser, this.Mapper);
        }

		/// <summary>
		/// constructor with a csv parser defined
		/// </summary>
		/// <param name="dataRows">is a <typeparamref name="string"/> The content of the file</param>
		/// <param name="delimiter">is a <typeparamref name="char"/> The delimiter used in file. Default value is ','</param>
		public CsvFile(List<string> dataRows, char delimiter = ',')
		{
			this.Parser = new CsvFileParser(delimiter);
			this.Mapper = new DataTypeService();
			this.Reader = new CsvReader(dataRows, this.Parser, this.Mapper);
		}
		#endregion
	}
}
